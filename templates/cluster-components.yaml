AWSTemplateFormatVersion: "2010-09-09"
Description: Course Project cluster's components. Version 1.0.0

Parameters:
  InstanceType:
    Type: String
    Description: ECS Cluster instance type
    Default: t2.micro
    AllowedValues:
      - t2.micro
      - t2.small
      - t2.medium
      - t3.micro
      - t3.small
      - t3.medium
      - t4g.micro
  MinClusterSizeOnDemand:
    Type: Number
    Description: Min ECS Cluster Size
    Default: 1
  MaxClusterSizeOnDemand:
    Type: Number
    Description: Max ECS Cluster Size
    Default: 2
  ClusterDesiredCapacityOnDemand:
    Type: Number
    Description: ECS Cluster desired capacity On Demand instances
    Default: 1
  KeyName:
    Type: AWS::EC2::KeyPair::KeyName
    Description: Key Pair for ECS instances
  MyIPAddress:
    Type: String
    Description: My IP Address to connect to SSH/HTTP

Mappings:
  RegionMap:
    ap-northeast-1:
      ECSImage: ami-070f7e54dfb7a2d5d
    ap-northeast-2:
      ECSImage: ami-0750461cbd3816b5c
    ap-south-1:
      ECSImage: ami-030cfb5ff193a8433
    ap-southeast-1:
      ECSImage: ami-03201e268e6830317
    ap-southeast-2:
      ECSImage: ami-0ad0ec0239add3bdd
    ca-central-1:
      ECSImage: ami-05f1e73f07a3d311a
    eu-central-1:
      ECSImage: ami-0363e2901d4206fe7
    eu-north-1:
      ECSImage: ami-03d3c9b522073e5d7
    eu-west-1:
      ECSImage: ami-0de9fc0f891440ef7
    eu-west-2:
      ECSImage: ami-01eff674c3f42d1ae
    eu-west-3:
      ECSImage: ami-0d8076085a324df63
    sa-east-1:
      ECSImage: ami-085745ee616882bfb
    us-east-1:
      ECSImage: ami-0a75316ee84844439
    us-east-2:
      ECSImage: ami-0517f843a0b05a590
    us-west-1:
      ECSImage: ami-05f915439746fc6ee
    us-west-2:
      ECSImage: ami-0b916fe5f37a6d65a

Resources:
  ecsCluster:
    Type: AWS::ECS::Cluster
    Properties:
      ClusterName: course-project-cluster

  ecsLaunchTemplateOnDemand:
    Type: AWS::EC2::LaunchTemplate
    Properties:
      LaunchTemplateName: course-project-ecs-cluster-ondemand
      LaunchTemplateData:
        ImageId:
          Fn::FindInMap:
            - RegionMap
            - !Ref "AWS::Region"
            - ECSImage
        KeyName: !Ref KeyName
        IamInstanceProfile:
          Name: !Ref ec2InstanceProfile
        InstanceType: !Ref InstanceType
        SecurityGroupIds:
          - !GetAtt ecsInstancesSecurityGroup.GroupId
        Monitoring:
          Enabled: true
        UserData:
          Fn::Base64: !Sub |
            #!/bin/bash -xe
            echo ECS_CLUSTER=${ecsCluster} >> /etc/ecs/ecs.config
            yum update -y
            yum install -y aws-cfn-bootstrap
            /opt/aws/bin/cfn-signal -e $? --stack ${AWS::StackName} --resource ecsAutoScalingGroupOnDemand --region ${AWS::Region}
        TagSpecifications:
          - ResourceType: instance
            Tags:
              - Key: Name
                Value: course-project-ecs-cluster

  ecsAutoScalingGroupOnDemand:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      AutoScalingGroupName: course-project-pushtoleave-ecs-ondemand-autoscaling-group
      LaunchTemplate:
        LaunchTemplateId: !Ref ecsLaunchTemplateOnDemand
        Version: !GetAtt ecsLaunchTemplateOnDemand.LatestVersionNumber
      VPCZoneIdentifier:
        - Fn::ImportValue: course-project-public-subnet-0
        - Fn::ImportValue: course-project-public-subnet-1
      MaxSize: !Ref MaxClusterSizeOnDemand
      MinSize: !Ref MinClusterSizeOnDemand
      DesiredCapacity: !Ref ClusterDesiredCapacityOnDemand
      MetricsCollection:
        - Granularity: 1Minute
          Metrics:
            - "GroupMinSize"
            - "GroupMaxSize"
      Tags:
        - Key: Name
          Value: course-project-ecs-cluster
          PropagateAtLaunch: true
    UpdatePolicy:
      AutoScalingRollingUpdate:
        MinInstancesInService: !Ref MinClusterSizeOnDemand
        MaxBatchSize: 1
        PauseTime: PT5M
    CreationPolicy:
      ResourceSignal:
        Timeout: PT5M

  ecsInstancesSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: ecs-instances-security-group
      GroupDescription: ECS Cluster security group
      VpcId:
        Fn::ImportValue: course-project-vpc

  ecsInstancesSecurityGroupALBAccess:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref ecsInstancesSecurityGroup
      IpProtocol: tcp
      FromPort: 31000
      ToPort: 61000
      SourceSecurityGroupId: !Ref albSecurityGroup

  ecsInstancesSecurityGroupSSH:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref ecsInstancesSecurityGroup
      IpProtocol: tcp
      FromPort: 22
      ToPort: 22
      CidrIp:
        Fn::ImportValue: course-project-vpc-cidr-block

  ecsInstancesSecurityGroupSSHFromDefinedIP:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref ecsInstancesSecurityGroup
      IpProtocol: tcp
      FromPort: 22
      ToPort: 22
      CidrIp: !Ref MyIPAddress

  albSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupName: course-project-alb-security-group
      GroupDescription: ALB security group
      VpcId:
        Fn::ImportValue: course-project-vpc

  albSecurityGroupHTTPInbound:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref albSecurityGroup
      IpProtocol: tcp
      FromPort: 80
      ToPort: 80
      CidrIp: !Ref MyIPAddress

  albSecurityGroupHTTPSInbound:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref albSecurityGroup
      IpProtocol: tcp
      FromPort: 443
      ToPort: 443
      CidrIp: !Ref MyIPAddress

  ExecutionRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: course-project-ecs-execution-role
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
      Policies:
        - PolicyName: "GetSsmParameters"
          PolicyDocument:
            Statement:
              - Effect: Allow
                Action:
                  - ssm:GetParameters
                Resource:
                  - !Sub arn:aws:ssm:${AWS::Region}:${AWS::AccountId}:parameter/dvwa-*

  TaskRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: course-project-ecs-task-role
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: "sts:AssumeRole"

  AutoScalingRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: course-project-ecs-autoscaling-role
      AssumeRolePolicyDocument:
        Statement:
          - Effect: Allow
            Principal:
              Service: ecs-tasks.amazonaws.com
            Action: "sts:AssumeRole"
      ManagedPolicyArns:
        - "arn:aws:iam::aws:policy/service-role/AmazonEC2ContainerServiceAutoscaleRole"

  ec2Role:
    Type: AWS::IAM::Role
    Properties:
      RoleName: course-project-ec2-role
      AssumeRolePolicyDocument:
        Version: "2012-10-17"
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - "ec2.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      Path: "/"
      Policies:
        - PolicyName: "root"
          PolicyDocument:
            Statement:
              - Effect: Allow
                Action:
                  - ecs:CreateCluster
                  - ecs:DeregisterContainerInstance
                  - ecs:DiscoverPollEndpoint
                  - ecs:Poll
                  - ecs:RegisterContainerInstance
                  - ecs:StartTelemetrySession
                  - ecs:Submit*
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                  - ecr:GetAuthorizationToken
                  - ecr:BatchCheckLayerAvailability
                  - ecr:GetDownloadUrlForLayer
                  - ecr:GetRepositoryPolicy
                  - ecr:DescribeRepositories
                  - ecr:ListImages
                  - ecr:DescribeImages
                  - ecr:BatchGetImage
                Resource: "*"

  ec2InstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      InstanceProfileName: course-project-ec2-instance-profile
      Path: "/"
      Roles:
        - !Ref ec2Role

  ecsServiceAutoscalingRole:
    Type: AWS::IAM::Role
    Properties:
      RoleName: course-project-ecs-service-autoscaling-role
      AssumeRolePolicyDocument:
        Statement:
          - Effect: "Allow"
            Principal:
              Service:
                - "application-autoscaling.amazonaws.com"
            Action:
              - "sts:AssumeRole"
      Path: "/"
      Policies:
        - PolicyName: "root"
          PolicyDocument:
            Statement:
              - Effect: "Allow"
                Action:
                  - application-autoscaling:*
                  - cloudwatch:DescribeAlarms
                  - cloudwatch:PutMetricAlarm
                  - ecs:DescribeServices
                  - ecs:UpdateService
                Resource: "*"

  LoadBalancer:
    Type: AWS::ElasticLoadBalancingV2::LoadBalancer
    Properties:
      LoadBalancerAttributes:
        # this is the default, but is specified here in case it needs to be changed
        - Key: idle_timeout.timeout_seconds
          Value: "60"
      Name: course-project-alb
      # "internal" is also an option
      Scheme: internet-facing
      SecurityGroups:
        - !Ref albSecurityGroup
      Subnets:
        - Fn::ImportValue: course-project-public-subnet-0
        - Fn::ImportValue: course-project-public-subnet-1
      Tags:
        - Key: Project
          Value: Course Project

  publicALBListener80:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      LoadBalancerArn: !Ref LoadBalancer
      Port: 80
      Protocol: HTTP
      DefaultActions:
        - Type: "redirect"
          RedirectConfig:
            Protocol: "HTTPS"
            Port: "443"
            Host: "#{host}"
            Path: "/#{path}"
            Query: "#{query}"
            StatusCode: "HTTP_301"

  publicALBListener443:
    Type: AWS::ElasticLoadBalancingV2::Listener
    Properties:
      LoadBalancerArn: !Ref LoadBalancer
      Port: 443
      Certificates:
        - CertificateArn:
            Fn::ImportValue: course-project-ssl-certificate
      Protocol: HTTPS
      DefaultActions:
        - Type: fixed-response
          FixedResponseConfig:
            ContentType: "text/plain"
            MessageBody: "Page not found"
            StatusCode: "404"
      SslPolicy: ELBSecurityPolicy-FS-1-2-Res-2020-10

Outputs:
  ecsCluster:
    Description: Logical ID of ECS Cluster
    Value: !Ref ecsCluster
    Export:
      Name: "course-project-cluster-logical-id"
  ecsAutoScalingRole:
    Description: Services autoscaling role
    Value: !GetAtt AutoScalingRole.Arn
    Export:
      Name: "course-project-ecs-service-autoscaling-role"
  ecsTaskRole:
    Description: Services role
    Value: !GetAtt TaskRole.Arn
    Export:
      Name: "course-project-ecs-service-role"
  ecsTaskExecutionRole:
    Description: Task execution role
    Value: !GetAtt ExecutionRole.Arn
    Export:
      Name: "course-project-ecs-task-execution-role"
  publicALBListener80:
    Description: Public ALB listener port 80 ARN
    Value: !Ref publicALBListener80
    Export:
      Name: "course-project-alb-listener-port-80"
  publicALBListener443:
    Description: Public ALB listener port 443 ARN
    Value: !Ref publicALBListener443
    Export:
      Name: "course-project-alb-listener-port-443"
  albSecurityGroup:
    Description: ALBs' Security Group
    Value: !GetAtt albSecurityGroup.GroupId
    Export:
      Name: "course-project-alb-security-group"
  albDnsName:
    Description: ALB DNS Name
    Value: !GetAtt LoadBalancer.DNSName
    Export:
      Name: "course-project-alb-dns-name"
  albCanonicalHostedZoneID:
    Description: ALB Canonical Hosted Zone ID
    Value: !GetAtt LoadBalancer.CanonicalHostedZoneID
    Export:
      Name: "course-project-alb-canonical-hosted-zone-id"
